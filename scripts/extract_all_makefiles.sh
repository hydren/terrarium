#!/bin/bash

# These need to be in par with the current build configuration names. Be sure that these are updated.
BUILD_CONFIG_ALLEGRO4=linux-gcc-allegro4-release
BUILD_CONFIG_ALLEGRO5=linux-gcc-allegro5-release
BUILD_CONFIG_SDL=linux-gcc-sdl-release
BUILD_CONFIG_SDL2=linux-gcc-sdl2-release
BUILD_CONFIG_SFML=linux-gcc-sfml-release
BUILD_CONFIG_SFML2=linux-gcc-sfml2-release

echo "- Calling script to extract makefiles for all release build configurations..."
for BUILD_CONFIG in $BUILD_CONFIG_ALLEGRO4 $BUILD_CONFIG_ALLEGRO5 $BUILD_CONFIG_SDL $BUILD_CONFIG_SDL2 $BUILD_CONFIG_SFML $BUILD_CONFIG_SFML2
do
	echo "- Dealing with $BUILD_CONFIG build configuration..."
	./scripts/extract_makefiles.sh $BUILD_CONFIG
	echo "Done dealing with $BUILD_CONFIG"
done
